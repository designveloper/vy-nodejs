var http = require('http');

var server = http.createServer(function(req, res) {
    //header: info about the response
    res.writeHead(200, {"Content-Type": "text/html"});
    //end the response, send message
    res.end(`
        <!DOCTYPE html>
        <html>
            <head>
                <title>Sample HTTP Module</title>
            </head>
            <body>
                <h1>HTTP Module Sample Response</h1>
                <p><strong>Request URL</strong>: ${req.url}</p>
                <p><strong>Request method</strong>: ${req.method}</p>
    `);
});

//listen on local machine
server.listen(3000)
console.log("Server is listening on port 3000.");
