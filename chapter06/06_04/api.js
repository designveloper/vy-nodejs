var http = require('http');
var data = require("./data/inventory");

http.createServer(function(req, res) {
    if (req.url === '/') {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(JSON.stringify(data));
    } else if (req.url === '/instock') {
        listInStock(res);
    } else if (req.url === '/onorder') {
        listOnBackOrder(res);
    } else {
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.end("404 Data File Not Found");
    }

}).listen(3000);

console.log("Listening to port 3000");

function listInStock(res) {
    //only return true or false, true: add to [array-name], false: not
    var inStock = data.filter(function(item) {
        return item.avail == "In stock";
    });
    res.end(JSON.stringify(inStock));
}

function listOnBackOrder(res) {
    var onOrder = data.filter(function(item) {
        return item.avail == "On back order";
    });
    res.end(JSON.stringify(onOrder));
}
