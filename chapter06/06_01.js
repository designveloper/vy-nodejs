var https = require('https');
var fs = require('fs');

var options = {
    hostname: "en.wikipedia.org",
    port: 443,
    path: "/wiki/Albert_Einstein",
    method: "GET"
};

var req = https.request(options, function(res){
    var responseBody = "";
    console.log("Response from server started.");
    console.log(`Server Status: ${res.statusCode} `);
    console.log("Response Headers: %j", res.headers);

    res.setEncoding("UTF-8");

    res.once("data", function(chunk){
        console.log(chunk);
    });
    res.on("data", function(chunk){
        console.log(`--chunk-- ${chunk.length}`);
        responseBody += chunk;
    });
    res.on("end", function(){
        fs.writeFile("albert-eistein.html", responseBody, function(err) {
            if(err) {
                throw err;
            }
            console.log("File downloaded.");
        });
    });
});

//catch error
req.on("error", function(err) {
    console.log(`Error: ${err.message}`);
});

req.end();
