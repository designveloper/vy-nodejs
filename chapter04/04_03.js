//assign the constructor to the variable EventEmitter
var EventEmitter = require('events').EventEmitter;
var util = require('util')

var Planet = function(name) {
    this.name = name;
}

//add the Planet obj to the prototype of the existing EventEmitter obj
// p will inherit all the function from e
util.inherits(Planet, EventEmitter);

var earth = new Planet("Earth");

earth.on('orbit', function(start){
    console.log(`${this.name} is orbitting ${start}.`);
})

earth.emit('orbit', "Sun");


/* throwaway code, but not throwaway comment

var emitter = new events.EventEmitter();

//will events occur - declare event, its name and when it will raise
emitter.on('customEvent', function(message, status){
    console.log(`${status}: ${message}`);
})

//fire the custom event, the second argument will play as the first argument of the event's callback function
emitter.emit('customEvent', "Hello World", 500);
*/
