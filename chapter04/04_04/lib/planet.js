var EventEmitter = require('events').EventEmitter;
var util = require('util');

var Planet = function(name) {
    this.name = name;
}

util.inherits(Planet, EventEmitter);

module.exports = Planet;
