var Planet = require("./lib/Planet")

var earth = new Planet("Earth");
var mars = new Planet("Mars");

earth.on('orbit', function(start){
    console.log(`${this.name} is orbitting ${start}.`);
})

mars.on('color', function(color){
    console.log(`${this.name} has the color of ${color}.`);
})

earth.emit('orbit', "Sun");
mars.emit('color', "red");
