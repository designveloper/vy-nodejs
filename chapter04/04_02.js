var readline = require('readline');

//readline will control stdin and stdout for us, saving us from having to control them directly
var rl = readline.createInterface(process.stdin, process.stdout);

var favBook = {
    title: '',
    quotes: []
};

rl.question("What is your favourite book?  ", function(answer){
    favBook.title = answer

    rl.setPrompt(`What is your favourite quote in ${favBook.title}?\n> `)
    rl.prompt();

    //trigger when user press an end-of-line input
    rl.on('line', function(quote){
        if(quote.toLowerCase().trim() === 'exit'){
            rl.close();
        } else{
            favBook.quotes.push(quote.trim());
            rl.setPrompt(`Do you have any favourite quote from ${favBook.title} to add? (type 'exit' to leave)\n> `);
            rl.prompt();
        }
    });
});

rl.on('close', function(){
    console.log(`\n${favBook.title} quotes: `);
    for (let quote of favBook.quotes){
        console.log(quote);
    }
    console.log("\n");
    process.exit();
})
