var path = require('path');
var util = require('util');
var v8 = require('v8');

console.log(path.basename(__filename));
//"The path.join() method joins all given path segments together using the platform specific separator as a delimiter, then normalizes the resulting path."
var dirUploads = path.join(__dirname, "www", 'files', 'uploads');

//add a date and time stamp
util.log(dirUploads);
//give info abour current memory statistics
util.log(v8.getHeapStatistics());
