# DSV - NodeJS

## What is NodeJS?
* Single-threaded
* Asynchronous
* Event are raised and recorded in an event queue and then handled in the order that then handled in the order that they were raised.

## Node core
* #### The global object
    * List of global objects that allow to use globally, without having to require anything: [here](https://nodejs.org/api/globals.html)
    * Every nodeJS file we create is it's own module, and every variable we create in that nodejs file is scoped only to that module, i.e. it will not be added to global obj.
* #### Process object
    * NodeJS `process` [documentation](https://nodejs.org/api/process.html)
    *  `process` objects
        * a global object
        * allow us to interact with info about current process instance
        * what it can do: get environment info, read environment var, communicate with the terminal or parent processes, exit the current processes,..
    * all the info we got from command prompt or terminal when the app starts will be stored in an objs called `process.argv`
    * **Standard input and standard output**
        * Offcial [documentation](https://nodejs.org/api/process.html#process_process_stdin)
        * Usage: communicate with the process while it is running
        * about `data` event and `readable` event on [stackoverflow](https://stackoverflow.com/questions/26174308/what-are-the-differences-between-readable-and-data-event-of-process-stdin-stream)
    * **Global timing functions**
        * `setTimeout`: create a delay of a certain time and then provoke the callback function
        * `setInterval`: call function like a heartbeat, i.e. wait for a certain time and then fire the callback function, again and again.. forever
        * `clearInterval`: stop the interval, must have the right id of the interval we need to stop, or it will not work

## Node Modules
* #### Core Modules
    * use `require()` to import modules
    * **Core modules**: install locally with the installation of NodeJS, without having to use npm
    * `path` [module](https://nodejs.org/api/path.html)
    * `util` [module](https://nodejs.org/api/util.html)
    * `v8` [module](https://nodejs.org/api/v8.html)
* #### Collecting information with Readline
    * `readline` [module](https://nodejs.org/api/readline.html)
    * allow us to ask questions of our terminal user
* #### Handling events with EventEmitter
    * [EventEmitter](https://nodejs.org/api/events.html#events_class_events_eventemitter)
    * NodeJS implementation of pub/sub design pattern (publish-subcribe pattern - [wiki](https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern))
    * allow us to create listeners for an emit custom events
    * events are handled asynchronously
* #### Exporting custom modules
    * NodeJS [modules](https://nodejs.org/api/modules.html)
    * `exports`: `module.exports` obj is the obj to be returned when we require the module (by the `require` statement).
* #### Creating child process
    * Child Process [module](https://nodejs.org/api/child_process.html)
    * Creating child process with `exec`: used for asynchronously synchronous process, perfect for process with little data
    * Creating child process with `spawn`: used for longer, ongoing process, with large amount of data

## The File System
* The `fs` module: [here](https://nodejs.org/api/fs.html)
* create file/dir, stream file, write file, read file, etc.. basically everything you need with a file system.
* when we use any methods of the `fs` module, we are given option to use them synchronously or asynchronously
* with asynchronous methods, we can use a callback function to check for errors. With synchronous, we can use `try`-`catch` to do so.
* remember to check whether file/directory exists before doing anything including it.
* `.once`: trigger one time only at the first event ever occurs
* the `line` event is also triggered by `return` aka backspace button, so be careful.

* **Listing directory files**
    * `fs.readdir[Sync]`
* **Reading files**
    * default is read in binary, make sure to tell the command the encoding.
    * `fs.readFile[Sync]`
    * statistic: `stat[Sync]` - check whether the obj is a file or a dir
* **Writing and appending files**
    * `fs.writeFile[Sync]`
    * `fs.appendFile[Sync]`
* **Directory Creation**
    * `fs.mkdir[Sync]`
    * `fs.existsSync` - `exists` was **deprecated**, use `fs.stat()` or `fs.access()` (*recommend*) instead
* **Renaming and removing files/directories**
    * `fs.rename[Sync]`: point to a different directory to move the file.
    *  `fs.unlink[Sync]`: used to delete files
    * we can also use `fs.rename[Sync]` to move directories
    * `fs.rmdir[Sync]`: used to delete directories. You can't remove a directory unless it is empty
* **Readable and writable file streams**
    * read and write data in chunk
    * [stream](https://nodejs.org/api/stream.html#stream_class_stream_duplex)
    * `fs.createReadStream`: create readable stream
    * `fs.createWriteStream`: create writable stream
    * remember to close stream

## The HTTP module
* #### Introduction:
    * HTTP [documentation](https://nodejs.org/api/http.html)
    * HTTPS [documentation](https://nodejs.org/api/https.html)
    * Usage: creating web server, making requests, handling responses,..
    * HTTPS module is for security server, require a security certificate.
    * [Anatomy of an HTTP transaction](https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/)
* #### Making a request
    * To make a request to a https page (i.g. wikipedia), you have to use HTTPS module
    * obj `option` include info about the request we'll make (port, hostname, method, path)
    * `http.request(option)`
* #### Building a web server
    * `http.createServer` - callback function:
        * first arg.: info about the request
        * second arg.: our responses
    * `(res/req).writeHead`: write the header (contain info about response/request);
    * `(res/req).end`
    * `listen`: specific the IP address and incoming port for requests
* #### Serving file
    * use `req.url` to find out what file to give user (use `req.url.match(/.css$/)` to check for css file, etc..)
    * use `fs.readFile` or ReadStream to read file and send respond (via readFile callback function)
    * use `res.writeHead` to add info about res.
    * with `readFile`: use `res.end` to send it
      with ReadStream: we can *pipe* a ReadStrem to a writable stream (i.e. our response obj) - `fileStream.pipe`
    * img is read in binary format
    * response content type:
        * text/html
        * text/css
        * text/plain
        * text/json
        * image/jpeg..
* #### Serving JSON data
    * APIs are used to serve data to client applications
    * JSON data file needs to be `stringify` before use `res.end` to send it.
* #### Collecting POST data
    * check the `req.method`

## Node Package Manager
    * [npm](https://npmjs.com)

## Web Servers
* #### The packet.json file
    * The packet.json: contain info about pj, keep track about project dependencies
    * `npm init` to initialize the packet.json file
    * `npm install [packet-name] --save` will save the package we install into the package.json (for later retrieving)
    * [cors](https://www.npmjs.com/package/cors)
    * when on other computers, first run `npm install` to install all the needed dependencies (*remember this*)
    * use `npm remove [package-name] --save` to remove package locally and inside the package.json
* #### Intro to express
    * middleware: customized plugins we can use to add functionality to application
    * `express.static` will invoke the static file server comes with express
* #### Express routing and CORS:
    * CORS (Cross-Origin Resource Sharing)
        * [mdn](https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS)
        * [wiki](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
        * different domains can't request resource (read) from each others due to same-origin security policy
* #### Express post bodies and params
    * handle `POST` method by using `body-parser` package
    * `request.params.[param-name]`

## WebSockets
* [WebSockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API)
* allow (true) 2 way connection between client and server
* client can send data easily broadcasted to every open connections, server can also push data changes to client(s)
* `require('ws').Server`
*  server event: `connection`, `message`
* `ws.send`: send message to client/server
* client event: `onopen`, `onclose`, `onmessage`
* more info and example [here](https://www.tutorialspoint.com//html5/html5_websocket.htm)
* [socket.io](https://socket.io/) - a module help us build websockets. If websocket is not supported, it will fall back to long poiling
    * socket.io is a function
    * require a socket.io-client js file to work on client (can download or install by `socket.io-client`)

## Testing and Debugging
* [mocha](http://mochajs.org/): decribing, building, running test
* [chai](http://chaijs.com/): work with values
* `nock`: create mock server
* any dependencies can, and should be mocked (to not affect the real data..)
* **SUT**: a module or function is being tested, short for "System Under Test". Other modules and functions out SUT dependent upon are referred to as "collaborators"
* `rewire`: inject mock
* [sinon](http://sinonjs.org/): create mock obj for test - make testing faster
    * **spies**: special kind of function that record details about how they are called, what arg. they are called with.. use to check if a function has been invoked (w/o invoke the real one)
    * **stub**: a more powerful spy. They allow you to control the behavior of a particular function. Use to invoke functions that behave a certain way
* `istanbul`: generate code coverage report
    * call `isbanbul cover [node_modules/mocha/bin/_mocha]`
    * replace [..] by the path to the mocha
    * in fact, just install mocha locally and use above, or else weird error might happen
* `supertest`: test HTTP app. Remeber to export the tested module
* `cheerio`: check the HTML DOM

## Automation and Deployment
* [grunt](https://gruntjs.com/)
* `grunt-contrib-jshint`
* `grunt-contrib-less`: convert less to css.
* `grunt-autoprefixer` add prefix to make sure our css works in many browsers as well
* [browserify](http://browserify.org/): allow to use CommonJS for client-side JS. Package up all the used modules and dependencies into a bundle file.
    * `grunt-browserify`
* `grunt-contrib-watch`: set up tasks to run when change and save files
* we can use npm to automatically run our files (edit the package.json file, in the `scripts` part), using grunt to check them for us, work in development mode,..
    * keyword: `start`, `test`, `publish`, `install`,..
    * make-up keywords has to have `run` before calling them
    * when we need to do something be4 doing some work (like `start`,..) we need to use `pre[work]` i.g. `prestart`
    * when we need to start something after, use `post` i.g. `poststart`
    * we can pack up many work in 1 keyworks, using `&`
    * use `npm-run-all` to run multiple npm-scripts in parallel or sequential
