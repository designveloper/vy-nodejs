var expect = require('chai').expect;
var tools = require("../lib/tools");

describe("Tools", function(){

    describe("printName()", function() {
        it("should print the last name first", function() {
            var results = tools.printName({ first: "Jane", last: "Harrington"});
            expect(results).to.equal("Harrington, Jane");
        });
    });

    //asynchronous
    //"done" (or whatever name it may be) is the function mocha will wait to run the test until it's invoked
    describe("loadWiki()", function() {
        //tell mocha to wait
        this.timeout(5000);

        it("should load the Albert Einstein wiki page", function(done) {
            tools.loadWiki({ first: "Albert", last: "Einstein"}, function(html){
                expect(html).to.be.ok;
                done();
            });
        });
    });
});
