var expect = require("chai").expect;
var rewire = require("rewire");

var order = rewire("../lib/order");
var sinon = require('sinon');

describe("Ordering Items", function() {

	beforeEach(function() {

		this.testData = [
			{sku: "AAA", qty: 10},
			{sku: "BBB", qty: 0},
			{sku: "CCC", qty: 3}
		];

		this.console = {
			//this is a function, but not the real one. it gives us detail about how the real was called
			//no semicolon here!
			log: sinon.spy()
		};

		this.warehouse = {
			//the yield invoke the callback sent to the real func, we could add arg. that it should send to the callback
			//and no semicolon here either!
			packageAndShip: sinon.stub().yields(84917271810)
		};

		order.__set__("inventoryData", this.testData);
		order.__set__("console", this.console);
		order.__set__("warehouse", this.warehouse);
	});

	it("order an item when there are enough in stock", function(done) {
		//the trick use to pass the tricky this usage
		var _this = this;

		order.orderItem("CCC", 3, function() {
			//console was replaced by the fake, so use this to check if the fake (in place of real) was actually called twice
			expect(_this.console.log.callCount).to.equal(2);

			done();
		});

	});

	describe("Warehouse interaction", function() {
		beforeEach(function() {
			this.callback = sinon.spy();
			order.orderItem("CCC", 2, this.callback);
		});

		it("should receive a tracking number", function(){
			expect(this.callback.calledWith(84917271810)).to.equal(true);
		});

		it("should calls packageAndShip with the correct sku and quantity", function() {
			expect(this.warehouse.packageAndShip.calledWith("CCC", 2)).to.equal(true);
		});
	});

});
