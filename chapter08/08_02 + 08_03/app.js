var express = require('express');
var cors = require('cors');
var app = express();

var skierTerms = [
    {
        term: "Berm",
        defined: " A term for a snowbank, often used to provide stability on the outside of a turn"
    },
    {
        term: "Carving",
        defined: "A series of clean turns using the edges of skis or a snowboard. Carving turns can vary from tight turns to giant “S” shaped swoops"
    },
    {
        term: "Edge",
        defined: "The sharpened metal strip on the sides of skis and snowboards, used for gaining control by biting into the snow for smoother carving and cutting. Holding an edge is a key to a good turn"
    },
    {
        term: "Glade",
        defined: "A stand of trees"
    },
    {
        term: "Milk Run",
        defined: "The first run early in the day"
    }
];

//custom middleware
//next: the funtion to be invoked when we finish
app.use(function(req, res, next) {
    console.log(`${req.method} request for '${req.url}''`);
    //tell application to move on to next piece of middleware (i.e. the epress static function below)
    next();
});

//if we make requests for any static file that under the public directory, they will be served
app.use(express.static("./public"));

//any domain can make request for dictionary-api
app.use(cors());

//set up the `get` route
app.get("/dictionary-api", function(req, res) {
    //thanks to express, we got this
    //take a json obj, handle stringifying it, set up the headers to reply with a json response
    res.json(skierTerms);
});

app.listen(3000);

console.log("Express is running on port 3000");

//to include this instance to other files
module.exports = app;
