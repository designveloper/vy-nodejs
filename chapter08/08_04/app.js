var express = require("express");
var cors = require("cors");
//parse the data posted
var bodyParser = require('body-parser');
var app = express();

var skierTerms = [
    {
        term: "Rip",
        defined: "To move at a high rate of speed"
    },
    {
        term: "Huck",
        defined: "To throw your body off of something, usually a natural feature like a cliff"
    },
    {
        term: "Chowder",
        defined: "Powder after it has been sufficiently skied"
    }
];

//parse data sent as json file
app.use(bodyParser.json());
//make sure if the body data was sent URL-encoded
//set to true if we have large amounts of nested POST data to parse
app.use(bodyParser.urlencoded({ extended: false}));

//all the data parsed will be pul neatly in req obj
app.use(function(req, res, next) {
	console.log(`${req.method} request for '${req.url} - ${JSON.stringify(req.body)}'`);
	next();
});

app.use(express.static("./public"));

app.use(cors());

app.get("/dictionary-api", function(req, res) {
	res.json(skierTerms);
});

//POST route
app.post("/dictionary-api", function(req, res) {
    skierTerms.push(req.body);
    res.json(skierTerms);
})

//DELETE route
//everything (route) behind the "/dictionary-api/" will be assigned to the variable "term"
app.delete("/dictionary-api/:term", function(req, res) {
    skierTerms = skierTerms.filter(function(item) {
        return (item.term.toLowerCase() !== req.params.term.toLowerCase());
    });
    res.json(skierTerms);
});

app.listen(3000);

console.log("Express app running on port 3000");

module.exports = app;
