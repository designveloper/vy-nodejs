var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
var fs = require("fs");

var favBook = {
    title: '',
    quotes: []
};


rl.question("What is your favourite book?  ", function(answer) {

	favBook.title = answer;

	var stream = fs.createWriteStream(favBook.title + ".md");
	stream.write(`${favBook.title}\n=================\n\n`);

	rl.setPrompt(`What is your favourite quote in ${favBook.title}?\n> `);
    rl.prompt();

    rl.on('line', function(quote){
        if(quote.toLowerCase().trim() === 'exit'){
			stream.close();
            rl.close();
        } else {
            favBook.quotes.push(quote.trim());
			stream.write(`* ${quote.trim()}\n`);

            rl.setPrompt(`Do you have any favourite quote from ${favBook.title} to add? (type 'exit' to leave)\n> `);
            rl.prompt();
        }
    });

});


rl.on('close', function() {
	console.log(`\n${favBook.title} quotes: `);
	for (let quote of favBook.quotes){
		console.log(quote);
	}
	console.log("\n");
	process.exit();

});
