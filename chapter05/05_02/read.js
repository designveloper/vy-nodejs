var fs = require('fs');
var path = require('path');

// //remember the encoding!!
// var contents = fs.readFile("./lib/sayings.md", "UTF-8", function(err, contents){
//     if (err){
//         console.log(err);
//     }
//     console.log(contents);
//     console.log("\n\n");
// });
//
// console.log("Reading file...\n");

/////////////////////////////////////////////////////////////

fs.readdir("./lib", function(err, files){
    if (err){
        console.log(err);
    }

    files.forEach(function(file){
        var fileDir = path.join(__dirname, "lib", file);
        var stats = fs.statSync(fileDir);
        if(stats.isFile()){
            fs.readFile(fileDir, "UTF-8", function(err, contents){
                console.log(`${file}\n`);
                console.log(contents);
                console.log("\n\n");
            });
        }
    });
});
