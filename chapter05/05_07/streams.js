var fs = require('fs');

//this could slow app, cause latency and impact memory, etc..
// fs.readFile("./chat.log", "UTF-8", function(err, chatLog) {
//     console.log(`File Read ${chatLog.length}`);
// });
//
// console.log("Reading file..");

var stream = fs.createReadStream("./chat.log", "UTF-8");
var data = "";

//listen one time
stream.once("data", function() {
    console.log("\n\n");
    console.log("Started reading file\n");
});

stream.on("data", function(chunk){
    process.stdout.write(`  chunk: ${chunk.length} | `)
    data += chunk;
});

stream.on("end", function(){
    stream.close();
    console.log("\n\n");
    console.log(`Finished reading file. Total read: ${data.length}\n`);
});
