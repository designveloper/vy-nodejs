var fs = require('fs');

// try {
//     fs.renameSync("./assets/logs", "./logs");
// } catch(err) {
//     console.log(err);
//     throw err;
// }
//
// console.log("Directory has been moved.");

////////////////////////////////////////////////////

// fs.rmdir("./assets", function(err){
//     if(err) {
//         throw err;
//     }
//     console.log("Assets directory has been removed.");
// });

////////////////////////////////////////////////////////

fs.readdirSync("./logs").forEach(function(fileName){
    fs.unlinkSync("./logs/" + fileName);
});

fs.rmdir("./logs", function(err){
    if(err) {
        throw err;
    }
    console.log("Logs directory has been removed.");
});
