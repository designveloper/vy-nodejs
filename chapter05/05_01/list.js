var fs = require('fs');

//this is synchronous, it will block other below activity
//var files = fs.readdirSync('./lib');

//this is asynchronous, whenever it finishes reading the file and dir, the func will then trigger
var files = fs.readdir('./lib', function(err, files){
    if (err) {
        throw err;
    }
    console.log(files);
});

//this will go first, due to asynchronous command
console.log("Reading files...");
