var fs = require('fs');

fs.renameSync("./lib/project-config.js", "./lib/config.json");

console.log("Project-config has been renamed.");

fs.rename("./lib/notes.md", "./notes.md", function(err){
    if (err){
        console.log(err);
    } else{
        console.log("Rename.md has been moved.");
    }
})
