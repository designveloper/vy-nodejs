var fs = require("fs");

var md = `
Sample Markdown Title
======================
Sample subtitle
----------------

* list item 1
* list item 2
* list item 3
`;

fs.writeFile("sample.md", md.trim(), function(err){
    console.log("\nFile created!");
});
