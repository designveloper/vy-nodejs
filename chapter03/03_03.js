var questions = [
    "What is your name?",
    "What is your favorite color?",
    "What is your favorite kind of sweet?"
];

var answers = [];

function ask(i){
    process.stdout.write(`\n\n${questions[i]}`);
    process.stdout.write(`\n> `);
}

//'data' event will be raised when the user types into the terminal
//and thus trigger the following activities
process.stdin.on('data', function(data){
    //the trim() method remove whitespaces from both ends of string
    answers.push(data.toString().trim());
    //as long as there's not enough answers to our question, the question will still go on.
    if (answers.length < questions.length){
        ask(answers.length);
    } else {
        //force the process to end, return to the terminal
        process.exit();
    }
})

//listen to exit event
process.on('exit', function(){
    process.stdout.write("\n\n");
    process.stdout.write(`Hello ${answers[0]}, would you like a ${answers[1]} ${answers[2]}?`);
})

ask(0);
