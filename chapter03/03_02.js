function grab(flag) {
    var index = process.argv.indexOf(flag);
    //return the next thing after the index we find in the array (which is the info we needed)
    //the index here is (obviously) the index of the flag
    //we dont want to return the flag, right?
    return (index === -1) ? null : process.argv[index+1];
}

var greeting = grab('--greeting');
var user = grab('--user');

if (!user || !greeting){
    console.log("Oops, something went wrong.");
} else {
    console.log(`Welcome ${user}, ${greeting}`);
}
