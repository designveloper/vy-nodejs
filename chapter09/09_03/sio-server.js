var express = require("express");
var http = require("http");
var app = express();
var server = http.createServer(app).listen(3000);
//socket.io is a func, so when you invoke it, you should send it the server it should listen to
var io = require("socket.io")(server);

//serve file when navigate to loacalhost:3000
app.use(express.static("./public"));

io.on("connection", function(socket) {

    socket.on("chat", function(message) {
        //emit an event to all connected socket
    	socket.broadcast.emit("message", message);
    });

    //actually emit an event
	socket.emit("message", "Welcome to Cyber Chat");

});

console.log("Starting Socket App - http://localhost:3000");
